<?php
require_once '../config/db.php';

class DAOProizvodjaci {
	private $db;

	private $GETALLPROIZVODJACI = "SELECT * FROM proizvodjaci";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getAllProizvodjaci(){

		$statement = $this->db->prepare($this->GETALLPROIZVODJACI);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

        
}
?>
