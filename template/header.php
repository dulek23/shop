<?php

    require_once '../korpa/controllerKorpa.php';
    $kc=new controllerKorpa();
    $artikli=$kc->korpaBrojAktikala();
    
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="../CSS/shop_style.css">
<link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-12 ">
                    <div id="header_logo_align">
                        <a href="../pocetna/viewPocetnaStranica.php" id="header_logo_style">
                            <span id="header_logo_title">Mobile Shop</span><br>
                            <span id="header_logo_subtitle">BRZO, LAKO, SIGURNO!</span>
                        </a>
                    </div>   
            </div>

            <div class="col-md-6 col-sm-12">
                <div id="header_nalog_link_position">
                    
                    <?php if(isset($_SESSION['id'])){ ?>
                    <div>
                        <span class="header_nalog_link">Zdravo, <?php echo $_SESSION['ime']; ?></span>
                        <a href="../korpa/?action=all" style="text-decoration: none;"><span id="header_korpa_link">Korpa <?php echo "(".$artikli.")"; ?></span></a><br>
                        <a href="../korisnici/?action=odjava" style="text-decoration: none;"><span class="header_nalog_link">Odjava</span></a>
                    </div>
                    <?php    }else{  ?>
                            <div>
                                <a href="../korisnici/viewMojNalog.php" style="text-decoration: none;">
                                    <span class="header_nalog_link">Moj nalog</span>
                                </a>
                            </div>
                    <?php    } ?>
                </div>
            </div>
            
        </div>
    </div>
    
</body>
</html>