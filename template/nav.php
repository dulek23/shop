<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="../CSS/shop_style.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  

</head>
<body>
    <div class="container-fluid" id="kontejner_padding">
                        
<nav class="navbar navbar-expand-md navbar-dark bg-dark mynav">  
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    
  <div class="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
      <div class="navbar-nav">
          <?php if(isset($_SESSION['id'])){ ?>
          <a class="nav-item nav-link active" href="../pocetna/viewPocetnaStranica.php"><span class="meni-linkovi">POČETNA</span></a>
          <a class="nav-item nav-link active" href="../telefoni/?action=all"><span class="meni-linkovi">TELEFONI</span></a>
          <a class="nav-item nav-link active" href="../tableti/?action=all"><span class="meni-linkovi">TABLETI</span></a>
          <a class="nav-item nav-link active" href="../maske/?action=all"><span class="meni-linkovi">MASKE</span></a>
          <a class="nav-item nav-link active" href="../narudzbine/?action=all"><span class="meni-linkovi">NARUDZBINE</span></a>
          <?php }else{ ?>
          <a class="nav-item nav-link active" href="../pocetna/viewPocetnaStranica.php"><span class="meni-linkovi">POČETNA</span></a>
          <a class="nav-item nav-link active" href="../telefoni/?action=all"><span class="meni-linkovi">TELEFONI</span></a>
          <a class="nav-item nav-link active" href="../tableti/?action=all"><span class="meni-linkovi">TABLETI</span></a>
          <a class="nav-item nav-link active" href="../maske/?action=all"><span class="meni-linkovi">MASKE</span></a>
          <?php } ?>
    </div>
  </div>

</nav>

    </div>
               
</body>
</html>