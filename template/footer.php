<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="../CSS/shop_style.css">
<link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid footer_background">
        <div class="row">
            
            <div class="col-md-4"">
                <div>
                    
                    <div class="footer_title">
                        <span>O NAMA</span>
                    </div>
                    
                    <div class="footer_justify_text pt-2 pl-4 pr-2">
                        <span>Mobile Shop je preduzeće koje se bavi uvozom, izvozom i distribucijom mobilnih telefona svih brendova.
                              U našoj ponudi možete pronaći veliki izbor mobilnih telefona vodećih svetskih marki po veoma povoljnim cenama.
                              Apple, Samsung, LG, Huawei i drugi.
                        </span>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                
                <div class="footer_title">
                    <span>TOP BRENDOVI</span>
                </div>
                
                <div class="d-flex justify-content-center pt-2">
                    <div style="color: white;">
                        <ul>
                        <li>Apple</li>
                        <li>Samsung</li>
                        <li>Huawei</li>
                        <li>LG</li>
                    </ul>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                
                <div class="footer_title">
                    <span>KONTAKT</span>
                </div>
                
                <div class="footer_box">
                    <div>
                        <span>Bulevar kralja Aleksandra 90</span>
                    </div>
                    
                    <div>
                        <span>11000 Beograd</span>
                    </div>
                    
                    <div>
                        <span>Telefon: 011/5648-125</span>
                    </div>
                    
                    <div>
                        <span>Radno vreme: 9-20h</span>
                    </div>
                    
                    <div>
                        <span>Subotom: 9-15h</span>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <hr id="footer_hr">
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div class="footer_box pb-4">
                    <span>Created by</span>
                    <span id="footer_copyright">Dusko Kovacevic</span>
                </div>
            </div>
        </div>
        
    </div>
</body>
</html>