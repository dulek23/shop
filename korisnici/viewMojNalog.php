<?php
    

    $login_username= isset($login_username)? $login_username:"";
    $login_password= isset($login_password)? $login_password:"";
    $msg= isset($msg)? $msg:"";
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    <title>Login kupca - Mobile Shop</title>
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <div class="row align-items-center" id="moj_nalog_content_height">
            
            <div class="col-md-6 col-sm-12 d-flex justify-content-center">
                <div>
                    <div class="pt-5 pl-3 pb-1"><span class="registracija_title">Registrovani korisnici</span></div>
                    <div class="pl-3 pb-4"><span>Ako kod nas imate korisnički nalog, molimo ulogujte se.</span></div>
                    <form method="post" action="../korisnici/">
                        <div class="pl-3 pb-1"><span>Username<span></div>
                        <div class="pl-3 pb-4 log_in_field"><input type="text" name="login_username" class="form-control"></div>
                        <div class="pl-3 pb-1"><span>Password</span></div>
                        <div class="pl-3 pb-4 log_in_field"><input type="password" name="login_password" class="form-control"></div>
                        <div class="pl-3 pb-2"><button type="submit" name="action" value="log_in" class="btn btn-primary">Prijavi se</button></div>
                    </form>
                    <div><span class="registracija_greska_ispis"><?php echo $msg;?></span></div>
                </div>
            </div>
            
            <div class="col-md-6 col-sm-12 d-flex justify-content-center">
                <div>
                    <div class="pt-5"><span class="registracija_title">Nemate nalog? Registrujte se</span></div>
                    <div><span>Kreiranjem naloga bićete u mogućnosti da:</span></div>
                    <div class="pt-2">
                        <ul>
                            <li>prolazite brže kroz proces plaćanja,</li>
                            <li>da koristite više adresa za isporuku,</li>
                            <li>pregledate i pratite Vaše porudžbine itd.</li>
                        </ul>
                    </div>
                    <div class="d-flex justify-content-center pb-5"><a class="btn btn-success" href="../korisnici/viewRegistracija.php"><span style="color: white;">Kreiraj korisnički nalog</span></a></div>
                </div> 
            </div>

        </div>
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>