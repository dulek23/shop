<?php
    
    require_once './controllerKorisnici.php';
    
    $action= isset($_REQUEST['action'])? $_REQUEST['action']:"";
    $kc=new controllerKorisnici();
    
    switch ($_SERVER['REQUEST_METHOD']){
        case "GET": 
            switch ($action){
                case "goregister":
                    $kc->goRegister();
                    break;
                
                case "odjava":
                    $kc->odjava();
                    break;
                /*
                case "gokorpa":
                    $kc->goKorpa();
                    break;
                */
            }
        break;
    
        case "POST":
            switch ($action){
                case "registracija":
                    $kc->registracija();
                    break;
                
                case "log_in":
                    $kc->logovanje();
                    break;
            }
        break;
    }
?>
