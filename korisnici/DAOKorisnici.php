<?php
require_once '../config/db.php';

class DAOKorisnici {
	private $db;

	private $INSERTKORISNIK  = "INSERT INTO KORISNICI (ime, prezime, username, email, lozinka) VALUES (?, ?, ?, ?, ?)";
        private $SELECTKORISNIKBYID = "SELECT * FROM korisnici WHERE id=?";
        private $SELECTKORISNIKBYUSERNAME = "SELECT *FROM korisnici WHERE username=?";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}



	public function insertKorisnik($ime, $prezime, $username, $email, $lozinka){

	    $statement = $this->db->prepare($this->INSERTKORISNIK);
	    $statement->bindValue(1, $ime);
	    $statement->bindValue(2, $prezime);
	    $statement->bindValue(3, $username);
            $statement->bindValue(4, $email);
            $statement->bindValue(5, $lozinka);
	    $statement->execute();
	}

        public function getKorisnikById($id){
            $statement = $this->db->prepare($this->SELECTKORISNIKBYID);
	    $statement->bindValue(1, $id);
            $statement->execute();
            $result=$statement->fetch();
            return $result;
        }
        
        public function getKorisnikByUsername($username){
            $statement= $this->db->prepare($this->SELECTKORISNIKBYUSERNAME);
            $statement->bindValue(1, $username);
            $statement->execute();
            $result=$statement->fetch();
            return $result;
        }

}
?>
