<?php
    $ime_err= isset($ime_err)? $ime_err:"";
    $prez_err= isset($prez_err)? $prez_err:"";
    $user_err= isset($user_err)? $user_err:"";
    $email_err= isset($email_err)? $email_err:"";
    $loz_err= isset($loz_err)? $loz_err:"";
    $pot_loz_err= isset($pot_loz_err)? $pot_loz_err:"";
    
    $ime= isset($ime)? $ime:"";
    $prezime= isset($prezime)? $prezime:"";
    $username= isset($username)? $username:"";
    $email= isset($email)? $email:"";
    
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    <title>Kreiraj novi korisnički nalog - Mobile Shop</title>
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <div class="row">
            <div class="p-5"><span class="registracija_title">Kreiraj korisnički nalog</span></div>
            <div class="col-12 d-flex justify-content-center">
                <div style="width: 310px;">
                    <div class="pb-4"><span style="font-size: 20px;">Lični podaci</span></div>
                    <form method="post" action="../korisnici/">
                        <div class="pb-2"><span>Ime</span></div>
                        <div><input type="text" name="ime" value="<?php echo $ime?>" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $ime_err;?></span></div>
                        
                        <div class="pb-2"><span>Prezime</span></div>
                        <div><input type="text" name="prezime" value="<?= $prezime?>" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $prez_err;?></span></div>
                        
                        <div class="pb-2"><span>Username</span></div>
                        <div><input type="text" name="username" value="<?= $username?>" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $user_err; ?></span></div>
                        
                        <div class="pb-2"><span>Email adresa</span></div>
                        <div><input type="text" name="email" value="<?= $email?>" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $email_err; ?></span></div>
                        
                        <div class="pb-4"><span style="font-size: 20px;">Podaci za prijavu</span></div>
                        
                        <div class="pb-2"><span>Lozinka</span></div>
                        <div><input type="password" name="lozinka" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $loz_err; ?></span></div>
                        
                        <div class="pb-2"><span>Potvrdi lozinku</span></div>
                        <div><input type="password" name="potvrda_lozinka" class="form-control"></div>
                        <div class="pb-4"><span class="registracija_greska_ispis"><?php echo $pot_loz_err; ?></span></div>
                        
                        <div class="d-flex justify-content-center pb-5"><button type="submit" name="action" value="registracija" class="btn btn-primary">Registruj se</button></div>
                    </form>  
                </div>
            </div>
        </div>
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>
