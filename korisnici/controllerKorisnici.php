<?php

    session_start();
    require_once './DAOKorisnici.php';
    
    class controllerKorisnici{
        
        function goRegister(){
            include './viewRegistracija.php';
        }
        
        function registracija(){
            $ime= isset($_POST['ime'])? $_POST['ime']:"";
            $prezime= isset($_POST['prezime'])? $_POST['prezime']:"";
            $username= isset($_POST['username'])? $_POST['username']:"";
            $email= isset($_POST['email'])? $_POST['email']:"";
            $lozinka= isset($_POST['lozinka'])? $_POST['lozinka']:"";
            $potvrda_lozinka= isset($_POST['potvrda_lozinka'])? $_POST['potvrda_lozinka']:"";
            $dao=new DAOKorisnici();
            $user=$dao->getKorisnikByUsername($username);
            if(!empty($ime) && !empty($prezime) && !empty($username) && !empty($email) && !empty($lozinka) && !empty($potvrda_lozinka)){
                if(!preg_match("/^[a-zA-z]*$/", $ime)){
                    $ime_err="Ovo polje mora sadržati samo slova!";
                    include './viewRegistracija.php';
                }
                 else if(!preg_match("/^[a-zA-z]*$/", $prezime)){
                    $prez_err="Ovo polje mora sadržati samo slova!";
                    include './viewRegistracija.php';
                }
                
                else if(!preg_match("/^[a-zA-Z0-9_.]*$/", $username)){
                    $user_err='Neispravan format! Zabranjeni specijalni znaci!';
                    include './viewRegistracija.php';
                }
                
                else if(strlen($username)<6){
                    $user_err='Molimo unesite 6 ili vise karaktera!';
                    include './viewRegistracija.php';
                }
                
                else if($user['username']==$username){
                    $user_err="Korisničko ime već postoji";
                    include './viewRegistracija.php';
                }
                
                else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $email_err='Neispravan format email adrese!';
                    include './viewRegistracija.php';
                }
                
                else if(strlen($lozinka)<8){
                    $loz_err="Lozinka mora imati najmanje 8 karaktera!";
                    include './viewRegistracija.php';
                }
                else if($lozinka!=$potvrda_lozinka){
                    $pot_loz_err="Lozinke nisu iste!";
                    include './viewRegistracija.php';
                }else{
                    $dao->insertKorisnik($ime, $prezime, $username, $email, md5($lozinka));
                    include './viewMojNalog.php';
                }
            }else{
                if(empty($ime)){
                   $ime_err='Ovo je obavezno polje!'; 
                }
                if(empty($prezime)){
                    $prez_err='Ovo je obavezno polje!';
                }
                if(empty($username)){
                   $user_err='Ovo je obavezno polje!'; 
                }
                if(empty($email)){
                   $email_err='Ovo je obavezno polje!'; 
                }
                if(empty($lozinka)){
                   $loz_err='Ovo je obavezno polje!'; 
                }
                if(empty($potvrda_lozinka)){
                   $pot_loz_err='Ovo je obavezno polje!'; 
                }
                include './viewRegistracija.php';
            }
        }
        
        function logovanje(){
            $login_username= isset($_POST['login_username'])? $_POST['login_username']:"";
            $login_password= isset($_POST['login_password'])? $_POST['login_password']:"";
            $dao=new DAOKorisnici();
            if(!empty($login_username) && !empty($login_password)){
                $user=$dao->getKorisnikByUsername($login_username);
                if($user['username']==$login_username){
                    if($user['lozinka']==md5($login_password)){
                        $user=$dao->getKorisnikByUsername($login_username);
                        $_SESSION['id']=$user['id'];
                        $_SESSION['ime']=$user['ime'];
                        header("Location: ../pocetna/viewPocetnaStranica.php");
                    }else{
                        $msg='Pogrešno korisničko ime ili lozinka!';
                        include './viewMojNalog.php';
                    }
                }else{
                    $msg='Pogrešno korisničko ime ili lozinka!';
                    include './viewMojNalog.php';
                }
            }else{
                $msg='Morate popuniti oba polja!';
                include './viewMojNalog.php';
            }
        }
        
        function odjava(){
            session_unset();
            session_destroy();
            include '../pocetna/viewPocetnaStranica.php';
        }
        
        function goKorpa(){
            include '../korpa/viewKorpa.php';
        }
        
    }
    
?>