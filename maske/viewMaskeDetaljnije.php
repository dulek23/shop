<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <div class="row">
            <div class="col-md-12 pt-3 pb-3">
                <div><span class="proizvodi_detaljnije_title"><?php echo $maska['opis'];?></span></div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-12 d-flex justify-content-center">
                <img src="../img/maske/<?php echo $maska['img'];?>" class="img-fluid">
            </div>
            
            <div class="col-md-6 col-sm-12 pt-4" style="margin: auto;">
                <div>
                    <div>
                        <div><span class="proizvodi_detaljnije_rok_isp">Očekivani rok isporuke:</span></div>
                        <div class="pb-3"><span class="proizvodi_detaljnije_rok">3 radna dana</span></div>
                    </div>
                
                    <div class="pt-3">
                        <div><span class="proizvodi_detaljnije_cena"><?php echo $maska['cena'];?> din.</span></div>
                    </div>
                    
                    
                    <div>
                        <div>
                            <?php if(isset($_SESSION['id'])){?>
                            <form method="post" action="../korpa/">
                                <div class="pt-2"><span>Količina:</span></div>
                                <div class="pt-1">
                                    <input type="number" name="kolicina" min="1" max="<?php echo $maska['kolicina']; ?>" value="1">
                                </div>
                                <input type="hidden" name="id_korisnika" value="<?php echo $_SESSION['id']; ?>">
                                <input type="hidden" name="id_proizvoda" value="<?php echo $maska['id']; ?>">
                                <input type="hidden" name="kategorija" value="maske">
                                <input type="hidden" name="model" value="<?php echo $maska['model']; ?>">
                                <input type="hidden" name="cena" value="<?php echo $maska['cena']; ?>">
                                <input type="hidden" name="img" value="<?php echo $maska['img']; ?>">
                                <div class="pt-3"><button type="submit" name="action" value="dodaj" class="btn btn-info">Dodaj u korpu</button></div>
                            </form>
                            <?php }else{ ?>
                            <a href="../korisnici/viewMojNalog.php" class="btn btn-info">Dodaj u korpu</a>   
                            <?php }?>
                        </div>
                    </div>
                   
                    <div class="pt-4 pb-5">
                        <span><?php echo $maska['opis_tekst'];?></span>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <div><span class="proizvodi_detaljnije_spec">Specifikacije i detalji</span></div>
                <div class="pt-2">
                    <table class="table table-bordered table-sm">
                        <tr>
                            <td class="proizvodi_detaljnije_table_column_left">Model</td>
                            <td style="width: 70%;"><?php echo $maska['naziv']." ".$maska['model'];?></td>
                        </tr>
                        
                        <tr>
                            <td class="proizvodi_detaljnije_table_column_left">Vrsta</td>
                            <td style="width: 70%;"><?php echo $maska['tip'];?></td>
                        </tr>
                        
                        <tr>
                            <td class="proizvodi_detaljnije_table_column_left">Boja</td>
                            <td style="width: 70%;"><?php echo $maska['boja'];?></td>
                        </tr>
                        

                    </table>
                </div>
            </div>
        </div>
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>