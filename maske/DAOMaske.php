<?php
require_once '../config/db.php';

class DAOMaske {
	private $db;

	private $GETCOUNTMASKEBYPROIZV = "SELECT COUNT(maske.id) 'broj' FROM proizvodjaci JOIN maske ON proizvodjaci.id=maske.id_proizvodjaca WHERE proizvodjaci.id=?";
        private $GETALLMASKE = "SELECT maske.*, proizvodjaci.naziv FROM maske JOIN proizvodjaci ON proizvodjaci.id=maske.id_proizvodjaca ORDER BY maske.id";
        private $GETMASKABYID = "SELECT maske.*, proizvodjaci.naziv FROM maske JOIN proizvodjaci ON proizvodjaci.id=maske.id_proizvodjaca WHERE maske.id=?";
        private $GETMASKEBYIDPROIZV= "SELECT maske.*, proizvodjaci.naziv FROM maske JOIN proizvodjaci ON proizvodjaci.id=maske.id_proizvodjaca WHERE maske.id_proizvodjaca=?";
        private $SELECTKOLICINA = "SELECT kolicina 'kol' FROM maske WHERE id=?";
        private $UPDATEKOLICINA = "UPDATE maske SET kolicina=kolicina-? WHERE id=?";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getBrojMasByProiz($id){

		$statement = $this->db->prepare($this->GETCOUNTMASKEBYPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getAllMaske(){
                $statement = $this->db->prepare($this->GETALLMASKE);
                $statement->execute();
		$result = $statement->fetchAll();
		return $result;
        }
        
        public function getMaskaById($id){

		$statement = $this->db->prepare($this->GETMASKABYID);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getMaskaByIdProizvodjaca($id){

		$statement = $this->db->prepare($this->GETMASKEBYIDPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
        
         public function getKolicinaMas($id){

		$statement = $this->db->prepare($this->SELECTKOLICINA);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function updateKolicinaMas($kolicina, $id){
                $statement= $this->db->prepare($this->UPDATEKOLICINA);
                $statement->bindValue(1, $kolicina);
                $statement->bindValue(2, $id);
                $statement->execute();
        }
}
?>

