<?php
    
    require_once '../proizvodjaci/DAOProizvodjaci.php';
    require_once './DAOMaske.php';

    class controllerMaske{
        
        function getProizvodjaci(){
            $dao=new DAOProizvodjaci();
            $proizvodjaci=$dao->getAllProizvodjaci();
            return $proizvodjaci;
        }
        
        function getMaske(){
            $dao=new DAOMaske();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $mas=$dao->getAllMaske();
            include './viewPrikazMaski.php';
        }
        
        function getMaskeDetalji(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOMaske();
            $maska=$dao->getMaskaById($id);
            include './viewMaskeDetaljnije.php';
        }
        
        function getMaskeByProizvodjac(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOMaske();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $mas=$dao->getMaskaByIdProizvodjaca($id);
            include './viewPrikazMaski.php';
        }
        
        function getCountMaskeByProizvodjac($id){
            $dao=new DAOMaske();
            $br=$dao->getBrojMasByProiz($id);
            return $br;
        }
        
    }
?>
