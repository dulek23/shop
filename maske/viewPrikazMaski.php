<?php

    
    require_once './controllerMaske.php';
    $masc=new controllerMaske();
    //print_r($br['broj']);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <div class="row">
            <div class="col-md-2" id="proizvodi_sidemenu_border">
                <div class="d-flex justify-content-center">
                    <nav class="navbar navbar-expand-md navbar-light" id="proizvodi_sidemenu_title">
                        <div class="d-flex justify-content-center">
                        <button class="navbar-toggler navbar-toggler-right" id="proizvodi_filter_button" type="button" data-toggle="collapse" data-target="#top-links" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span>Filter</span>
                        </button>
                        </div>
                        <div class="collapse navbar-collapse" id="top-links">
                            <div class="navbar-nav flex-column">
                                <div class="d-flex justify-content-center pt-4"><span>Proizvođač</span></div>
                                <div>
                                    <div id="proizvodi_sidemenu_links">
                                        <?php foreach ($proizvodjaci as $p){ ?>
                                        <?php $br=$masc->getCountMaskeByProizvodjac($p['id']); ?>
                                        <div class="d-flex justify-content-center pb-1"><a class="nav-item" href="../maske/?action=sortiraj&id=<?php echo $p['id']; ?>"><?php echo $p['naziv']." (".$br['broj'].")"; ?></a></div>
                                        <?php }?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>

            <div class="col-md-10 col-sm-12 pt-3" style="min-height: 650px;">
                <div class="row">
                    <div class="col-12">
                        <!--for petlja ispisuje telefone iz baze -->
                        <?php foreach ($mas as $m){?>
                        <div class="d-flex justify-content-center pb-3 pt-3" id="proizvodi_border_bottom" >
                            <div class="d-flex justify-content-center" id="proizvodi_img_position">
                                <div>
                                    <img src="../img/maske/<?php echo $m['img'];?>" id="proizvodi_img">
                                </div>
                            </div>

                            <div class="d-flex" id="proizvodi_karakeristike_div">
                                <div>
                                    <div>
                                        <div><span id="proizvodi_title"><?php echo $m['opis']; ?></span></div>
                                        <div><span class="proizvodi_karakeristike">Šifra artikla: <?php echo $m['id']+300000; ?></span></div>
                                    </div>

                                    <div class="d-none d-sm-block">
                                        <div>
                                            <span class="proizvodi_karakeristike">Vrsta: </span>
                                            <span class="proizvodi_karak_tekst"><?php echo $m['tip']; ?></span>
                                        </div>
                                        <div>
                                            <span class="proizvodi_karakeristike">Namena: </span>
                                            <span class="proizvodi_karak_tekst"><?php echo $m['model']; ?></span>
                                        </div>
                                        <div>
                                            <span class="proizvodi_karakeristike">Boja: </span>
                                            <span class="proizvodi_karak_tekst"><?php echo $m['boja']; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex justify-content-center" id="proizvodi_cena_div">
                                <div>
                                    <div><span id="proizvodi_cena"><?php echo $m['cena'];?> din.</span></div>
                                    <div><a href="../maske?action=detalji&id=<?php echo $m['id']; ?>" class="btn btn-primary">Detaljnije</a></div>
                                </div>
                            </div>
                        </div>
                        <?php  }?>
                        <!--kraj for petlje -->
                        
                    </div>
                </div>
            </div>

        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>