<?php

    if(!isset($_SESSION)) 
    { 
        session_start(); 
    }

    require_once 'DAOKorpa.php';
    require_once '../telefoni/DAOTelefoni.php';
    require_once '../tableti/DAOTableti.php';
    require_once '../maske/DAOMaske.php';
    
    class controllerKorpa{
        
        
        function selectAllKorpa(){
            if(isset($_SESSION['id'])){
            $dao=new DAOKorpa();
            $id_korisnika=$_SESSION['id'];
            $korpa=$dao->selectAll($id_korisnika);
            $red=$dao->countIdKorpa($id_korisnika);
            $ukupno_cena=$dao->korpaUkupno($id_korisnika);
            include './viewKorpa.php';
            }
        }
        
        function deleteFromKorpa(){
            if(isset($_SESSION['id'])){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOKorpa();
            $dao->deleteProizvod($id);
            $id_korisnika=$_SESSION['id'];
            $korpa=$dao->selectAll($id_korisnika);
            $red=$dao->countIdKorpa($id_korisnika);
            $ukupno_cena=$dao->korpaUkupno($id_korisnika);
            include './viewKorpa.php';
            }
        }
        
        function insertInKorpa(){
            if(isset($_SESSION['id'])){
            $id= isset($_POST['id'])? $_POST['id']:"";
            $kategorija= isset($_POST['kategorija'])? $_POST['kategorija']:"";
            $id_korisnika= isset($_POST['id_korisnika'])? $_POST['id_korisnika']:"";
            $id_proizvoda= isset($_POST['id_proizvoda'])? $_POST['id_proizvoda']:"";
            $kolicina = isset($_POST['kolicina'])? $_POST['kolicina']:"";
            $model = isset($_POST['model'])? $_POST['model']:"";
            $cena = isset($_POST['cena'])? $_POST['cena']:"";
            $img = isset($_POST['img'])? $_POST['img']:"";
            $ukupno=$kolicina*$cena;
            $dao=new DAOKorpa();
            $dao->insertInKorpa($kategorija, $kolicina, $id_korisnika, $id_proizvoda, $cena, $model, $img, $ukupno);
            $id_korisnika=$_SESSION['id'];
            $korpa=$dao->selectAll($id_korisnika);
            $red=$dao->countIdKorpa($id_korisnika);
            $ukupno_cena=$dao->korpaUkupno($id_korisnika);
            include '../korpa/viewKorpa.php';
            }
        }
        
        function kolicina($kategorija, $id){
            $daotel=new DAOTelefoni();
            $daotab=new DAOTableti();
            $daomas=new DAOMaske();
            if($kategorija=="telefoni"){
                $kolicina=$daotel->getKolicinaTel($id);
            }else if($kategorija=="tableti"){
                $kolicina=$daotab->getKolicinaTab($id);
            }else if($kategorija=="maske"){
                $kolicina=$daomas->getKolicinaMas($id);
            }
            return $kolicina['kol'];    
        }
        
        function updateKolicina(){
            if(isset($_SESSION['id'])){
            $id= isset($_POST['id'])? $_POST['id']:"";
            $id_korisnika=$_SESSION['id'];
            $kolicina= isset($_POST['kolicina'])? $_POST['kolicina']:"";
            $dao=new DAOKorpa();
            $dao->updateKolicina($kolicina, $id);
            $dao->cenaUkupnoByProizvod($id);
            $ukupno_cena=$dao->korpaUkupno($id_korisnika);
            $korpa=$dao->selectAll($id_korisnika);
            $red=$dao->countIdKorpa($id_korisnika);
            include '../korpa/viewKorpa.php';
            }
        }
      
        function truncateKorpa(){
            $dao=new DAOKorpa();
            $dao->truncateKorpa();
        }
        
        function korpaBrojAktikala(){
            if(isset($_SESSION['id'])){
             $dao=new DAOKorpa();
             $id_korisnika=$_SESSION['id'];
             $red=$dao->countIdKorpa($id_korisnika);
             return $red['broj'];
            }
        }
        
  }
?>