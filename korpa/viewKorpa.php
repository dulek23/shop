<?php
    require_once 'controllerKorpa.php';
    $kc=new controllerKorpa();
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        
        <!-- ispis proizvoda koji su dodati u korpu -->
        <?php if($red['broj']==0){ ?>
        <div class="row">
            <div class="col-12">
                <div style="margin: auto;">
                    <div class="d-flex justify-content-center pt-5"><img src="../img/cart.svg" id="korpa_empty_img"></div>
                    <div class="d-flex justify-content-center pb-2" id="korpa_empty_title"><span>Korpa je prazna</span></div>
                    <div class="d-flex justify-content-center"><span>Nemate artikala u Vašoj korpi</span></div>
                    <div class="d-flex justify-content-center pb-5"><span>Kliknite <a href="../pocetna/viewPocetnaStranica.php" id="korpa_empty_link">ovde</a> da nastavite sa kupovinom.</span></div>
                </div>
            </div>
        </div>
        <?php }else{?>
        
        
        <div class="row">
            <div class="col-12">
                <div class="p-4">
                    <span id="korpa_title">Korpa</span>
                </div>
            </div>
        </div>
               
        <div class="row" id="korpa_row_header" >
            <div class="col-md-1 d-sm-none d-md-block">
                <div style="width: 100%;"></div>
            </div>
            
            <div class="col-md-5 d-sm-none d-md-block">
                <div class="d-none d-sm-block">
                    <div><span>Naziv proizvoda</span></div>
                </div>
            </div>
            
            <div class="col-md-2 d-sm-none d-md-block">
                <div class="d-none d-sm-block">
                    <div><span>Pojedinačna cena:</span></div>
                </div>
            </div>
            
            <div class="col-md-2 d-sm-none d-md-block">
                <div class="d-none d-sm-block">
                    <div><span>Količina:</span></div>
                </div>
            </div>
            
            <div class="col-md-2 d-sm-none d-md-block">
                <div class="d-none d-sm-block">
                    <div><span>Ukupno:</span></div>
                </div>
            </div>
        </div>
        <?php foreach ($korpa as $k){ ?>
        <div class="row" id="korpa_proizvod_border_bottom">
            <div class="col-md-1 ">
                <div><img src="../img/<?php echo $k['kategorija']?>/<?php echo $k['img']; ?>" id="korpa_proizvod_img"></div>
            </div>
            
            <div class="col-md-5">
                <div>
                    <span><?php echo $k['model'] ?></span>
                </div>
            </div>
            
            <div class="col-md-2 d-sm-none d-md-block">
                <div class="d-none d-sm-block">
                    <span><?php echo $k['cena']; ?> din.</span>
                </div>
            </div>

            <div class="col-md-2">
                <div class="pt-1">
                    <form method="post" action="../korpa/">
                        <input type="number" name="kolicina" value="<?php echo $k['kolicina']; ?>" min="1" max="<?php echo $kc->kolicina($k['kategorija'], $k['id_proizvoda']); ?>">
                        <input type="hidden" name="id" value="<?php echo $k['id']; ?>">
                        <button type="submit" name="action" value="izmeni" class="btn btn-light">Izmeni</button>
                    </form>
                </div>
            </div>
            
            <div class="col-md-1">
                <div>
                    <span><?php echo $k['kolicina']*$k['cena']; ?> din.</span>
                </div>
            </div>
            
            <div class="col-md-1">
                <div class="pl-2">
                    <a href="../korpa/?action=delete&id=<?php echo $k['id']; ?>"><img src="../img/delete1.svg" style="width: 30px;"></a>
                </div>
            </div>
        </div>

        <?php }?>
        
        <div class="row">
            <div class="col-12">
                <div>
                    <div class="float-right" style="padding-right: 150px;"><span>Ukupno: <?php echo $ukupno_cena['korpa_ukupno'] ?> din.</span></div>
                </div>
            </div>
        </div>
        
        <div class="row pt-2 pb-3">
            <div class="col-12">
                <div class="float-right" style="padding-right: 150px;">
                    <form method="post" action="../narudzbine/">

                        <button type="submit" name="action" value="naruci" class="btn btn-success">Naruči</button>
                    </form>
                </div>
            </div>
        </div>
        <?php }?>
    
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>