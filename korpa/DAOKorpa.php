<?php
require_once '../config/db.php';

class DAOKorpa {
	private $db;

        private $INSERTINKORPA = "INSERT INTO korpa (kategorija, kolicina, id_korisnika, id_proizvoda, cena, model, img, ukupno) VALUES (?,?,?,?,?,?,?,?)";
        private $UPDATEKOLICINA = "UPDATE korpa SET kolicina=? WHERE id=?";
        private $DELETEFROMKORPA = "DELETE FROM korpa WHERE id=?";
        private $COUNTROWSKORPA = "SELECT COUNT(id) 'broj' FROM korpa WHERE id_korisnika=?";
        private $UPDATEBROJKUPOVINE = "UPDATE korpa SET broj_kupovine=broj_kupovine+1 WHERE id_korisnika=?;";
        
        private $SELECTALL = "SELECT * FROM korpa WHERE id_korisnika=?";
        private $SELECTROWKORPA = "SELECT *FROM korpa WHERE id=?";
        private $SELECTUKUPNO = "UPDATE korpa SET ukupno=kolicina*cena WHERE id=?";
        private $KORPAUKUPNO = "SELECT SUM(ukupno) 'korpa_ukupno' FROM korpa WHERE id_korisnika=?";
        private $TRUNCATEKORPA = "TRUNCATE TABLE korpa";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function insertInKorpa($kategorija, $kolicina, $id_korisnika, $id_proizvoda, $cena, $model, $img, $ukupno){

		$statement = $this->db->prepare($this->INSERTINKORPA);
                $statement->bindValue(1, $kategorija);
                $statement->bindValue(2, $kolicina);
                $statement->bindValue(3, $id_korisnika);
                $statement->bindValue(4, $id_proizvoda);
                $statement->bindValue(5, $cena);
                $statement->bindValue(6, $model);
                $statement->bindValue(7, $img);
                $statement->bindValue(8, $ukupno);
		$statement->execute();
	}

	public function updateKolicina($kolicina, $id){

		$statement = $this->db->prepare($this->UPDATEKOLICINA);
                $statement->bindValue(1, $kolicina);
                $statement->bindValue(2, $id);
		$statement->execute();        
        }
        
        public function deleteProizvod($id){
                $statement = $this->db->prepare($this->DELETEFROMKORPA);
                $statement->bindValue(1, $id);
                $statement->execute();
        }
        
        public function countIdKorpa($id){
                $statement= $this->db->prepare($this->COUNTROWSKORPA);
                $statement->bindValue(1,$id);
                $statement->execute();
                $result = $statement->fetch();
                return $result;
        }
        
         public function selectAll($id){
                $statement = $this->db->prepare($this->SELECTALL);
                $statement->bindValue(1, $id);
                $statement->execute();
		$result = $statement->fetchAll();
		return $result;
        }
        
        public function getProizvodById($id){

		$statement = $this->db->prepare($this->SELECTROWKORPA);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function cenaUkupnoByProizvod($id){
            
                $statement = $this->db->prepare($this->SELECTUKUPNO);
                $statement->bindValue(1, $id);
		$statement->execute();
        }
        
        public function korpaUkupno($id){
            
                $statement= $this->db->prepare($this->KORPAUKUPNO);
                $statement->bindValue(1, $id);
                $statement->execute();
                $result = $statement->fetch();
                return $result;
        }
        
        public function truncateKorpa(){
                $statement= $this->db->prepare($this->TRUNCATEKORPA);
                $statement->execute();
        }
}
?>
