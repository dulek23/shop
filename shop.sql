-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2020 at 01:36 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id` int(11) NOT NULL,
  `ime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id`, `ime`, `prezime`, `username`, `email`, `lozinka`) VALUES
(14, 'Dusko', 'Kovacevic', 'dulek23', 'duskokovacevic47@gmail.com', '22d7fe8c185003c98f97e5d6ced420c7'),
(16, 'Marko', 'Markovic', 'marko123', 'test@test.com', '22d7fe8c185003c98f97e5d6ced420c7');

-- --------------------------------------------------------

--
-- Table structure for table `korpa`
--

CREATE TABLE `korpa` (
  `id` int(11) NOT NULL,
  `kategorija` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `kolicina` int(11) NOT NULL,
  `id_korisnika` int(11) NOT NULL,
  `id_proizvoda` int(11) NOT NULL,
  `cena` int(10) NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ukupno` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maske`
--

CREATE TABLE `maske` (
  `id` int(11) NOT NULL,
  `model` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cena` int(20) NOT NULL,
  `img` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `tip` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `boja` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `kolicina` int(20) NOT NULL,
  `opis` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `opis_tekst` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proizvodjaca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `maske`
--

INSERT INTO `maske` (`id`, `model`, `cena`, `img`, `tip`, `boja`, `kolicina`, `opis`, `opis_tekst`, `id_proizvodjaca`) VALUES
(2, 'Alcatel M POP FLIP white za 5020X', 299, 'alcatel_m_pop_bela.jpg', 'Zaštitna maska', 'Bela', 30, 'Zaštitna maska Alcatel M POP FLIP white za 5020X', 'Zaštitna maska na preklop koja će zaštiti vaš Alcatel 5020X, od prašine i mogućih ogrebotina i oštećenja nastalih prilikom slučajnih padova.', 2),
(4, 'Y6 Madrid', 990, 'y6_madrid.jpg', 'Zaštitna futrola', 'Transparent', 30, 'Zaštitna maska HUAWEI Y6 Madrid', 'Idealna zaštita za Vaš Huawei Y6 mobilni telefon. Polomljeni okvir i staklo su prošlost uz ovu sjajnu zaštitu.', 3),
(5, 'iPhone 11', 7190, 'iphone11.jpg', 'Silikonska zaštitna maska za iPhone 11', 'Providna', 30, 'Apple iPhone 11 Clear Case, mwvg2zm/a', 'Apple iPhone 11 Clear Case, mwvg2zm/a', 1),
(6, 'iPhone 8/7', 2990, 'iphone8.jpg', 'Zaštitna maska', 'Crna', 30, 'Zaštitna maska Apple iPhone 8/7 Silicone Case - Black, mqgk2zm/a', 'Zaštitna maska Apple iPhone 8/7 Silicone Case - Black, mqgk2zm/a', 1),
(7, 'A50 GRADATION COVER', 1999, 'a50_roze.png', 'Zaštitna maska', 'Roze', 30, 'SAMSUNG zaštitna maska A50 GRADATION COVER - EF-AA505-CPE Samsung Galaxy A50 , Roze', 'SAMSUNG zaštitna maska A50 GRADATION COVER', 4),
(8, 'A50 GRADATION COVER', 1999, 'a50_plava.png', 'Zaštitna maska', 'Ljubičasta', 30, 'SAMSUNG zaštitna maska A50 GRADATION COVER - EF-AA505-CPE Samsung Galaxy A50 , LJubicasta', 'SAMSUNG zaštitna maska A50 GRADATION COVER', 4);

-- --------------------------------------------------------

--
-- Table structure for table `narudzbine`
--

CREATE TABLE `narudzbine` (
  `id` int(11) NOT NULL,
  `kategorija` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `kolicina` int(11) NOT NULL,
  `id_korisnika` int(11) NOT NULL,
  `id_proizvoda` int(11) NOT NULL,
  `cena` int(10) NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ukupno` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proizvodjaci`
--

CREATE TABLE `proizvodjaci` (
  `id` int(11) NOT NULL,
  `naziv` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proizvodjaci`
--

INSERT INTO `proizvodjaci` (`id`, `naziv`) VALUES
(1, 'Apple'),
(2, 'Alcatel'),
(3, 'Huawei'),
(4, 'Samsung');

-- --------------------------------------------------------

--
-- Table structure for table `tableti`
--

CREATE TABLE `tableti` (
  `id` int(11) NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cena` int(10) NOT NULL,
  `img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `velicina_ekrana` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rezolucija` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `procesor` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `radna_memorija` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `interna_memorija` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mem_kartica` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kamera` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pred_kamera` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `oper_sistem` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bluetooth` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `wifi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `baterija` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kolicina` int(5) NOT NULL,
  `boja` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dimenzije` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `masa` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `opis` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `opis_tekst` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proizvodjaca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tableti`
--

INSERT INTO `tableti` (`id`, `model`, `cena`, `img`, `velicina_ekrana`, `rezolucija`, `procesor`, `radna_memorija`, `interna_memorija`, `mem_kartica`, `kamera`, `pred_kamera`, `oper_sistem`, `bluetooth`, `wifi`, `baterija`, `kolicina`, `boja`, `dimenzije`, `masa`, `opis`, `opis_tekst`, `id_proizvodjaca`) VALUES
(1, '1T', 11889, 'alcatel_1t.png', '10.1\"', '1280 x 800', 'Quad-Core 1.3 G Hz CPU, MT8321', '1GB', '16GB', 'microSD (TransFlash) do 128GB', '2.0 Mpix', '2.0 Mpix', 'Android 8.1 Oreo', 'Da', '802.11 b/g/n', 'Lithium ion 4000mAh', 30, 'crna', '255 x 155 x 9.35 mm', '415g', 'ALCATEL 1T 10\" (crni), 10.1\", Četiri jezgra, 1GB, WiFi', 'ALCATEL 1T 10\"', 2),
(2, 'Mediapad T3 10', 17999, 'huawei_mediapad_t3.png', '9.6\"', '1280 x 800', 'Quad-core 1.4 GHz Cortex-A53, Qualcomm MSM8917 Snapdragon 425', '2 GB', '16GB', 'microSD (TransFlash) do 128GB', '5.0 Mpix', '2.0 Mpix', 'Android 7.0 Nougat', 'Da', 'Wi-Fi 802.11 b/g/n or a/b/g/n, Wi-Fi Direct', 'Lithium ion 4800mAh', 30, 'crna', '229.8 x 159.8 x 8 mm', '460g', 'HUAWEI Mediapad T3 10\" (Siva) 9.6\", Četiri jezgra, 2GB, WiFi', 'Dodirnite moć briljantnog dizajna pomoću svog HUAWEI MediaPad T3 10. Pogledajte svet u živopisnoj boji kroz ekran od 9,6 inča. Osetite kvalitet u jednodelnim anodiziranim aluminijumskim kućištem.', 3),
(3, 'iPad 7', 53299, 'ipad7.png', '10.2\"', '2160 x 1620', 'Apple A10 Integrisani M10 koprocesor', '6 GB', '32 GB', 'Ne', '8.0 Mpix', '1.2 Mpix', 'iPad OS', 'Da', 'Wi-Fi (802.11a/b/g/n/ac)', 'Lithium ion 4000mAh', 30, 'Tamno siva', '250.6 x 174.1 x 7.5 mm', '483 g', 'APPLE iPad 7 10.2\" WiFi 32 GB Space gray (sivi) - MW742HC/A, 10.2\", Četiri jezgra, WiFi', 'APPLE iPad 7 10.2\" WiFi 32 GB', 1),
(4, 'Galaxy Tab S6 LTE', 97999, 'samsung_tab_s6.png', '10.5\"', '2560 x 1600', 'Octa-core (1x2.84 GHz Kryo 485 & 3x2.42 GHz Kryo 485 & 4x1.78 GHz Kryo 485)', '6GB', '128GB', 'microSD (TransFlash) do 1TB', '13.0 Mpix', '5.0 Mpix', 'Android 9.0 Pie', 'Da', 'Wi-Fi: 802.11 a/b/g/n/ac 2.4G+5GHz', 'Lithium ion 7040mAh', 30, 'Siva', '244.5 x 159.5 x 5.7 mm', '420 g', 'SAMSUNG Galaxy Tab S6 LTE Gray (sivi) - SM-T865NZAASEE, 10.5\", Osam jezgara, 6GB', 'SAMSUNG Galaxy Tab S6 LTE ', 4),
(5, 'iPad Pro', 259990, 'ipad_pro.png', '12.9\"', '2732 x 2048', 'Apple A12X Bionic 4x 2.5 GHz Vortex, 4x 1.59 GHz Tempest', '6 GB', '1TB', 'Ne', '12.0 Mpix', '7.0 Mpix', 'Apple iOS 12', 'Da', 'Wi‑Fi (802.11a/b/g/n/ac)', 'Lithium ion 9720mAh', 30, 'Srebrna', '280.6 x 214.9 x 5.9 mm', '631g', 'APPLE iPad Pro 12.9\" WiFi 1TB (Srebrni - Silver) - MTFT2HC/A 12.9\", Osam jezgara, 6GB, WiFi', 'APPLE iPad Pro 12.9\" WiFi 1TB Silver', 1),
(6, 'Galaxy Tab A 10.1', 37999, 'galaxy_tab_A.png', '10.1\"', '1920 x 1200', 'Exynos 7 Octa 7870 (Octa-core 1.6 GHz Cortex-A53)', '2 GB', '32 GB', 'microSD (TransFlash) do 200GB', '8.0 Mpix', '2.0 Mpix', 'Android 6.0.1 Marshmallow', 'Bluetooth v4.2', '802.11 a/b/g/n/ac 2.4G+5GHz, VHT80', 'Lithium ion 7300mAh', 30, 'Crna', '155.3 x 254.2 x 8.2mm', '525g', 'SAMSUNG Galaxy Tab A 10.1 LTE (2016) (Crni) - SM-T585NZKESEE 10.1\", Osam jezgara, 2GB, 4G/WiFi', 'SAMSUNG Galaxy Tab A 10.1 LTE (2016) (Crni)', 4);

-- --------------------------------------------------------

--
-- Table structure for table `telefoni`
--

CREATE TABLE `telefoni` (
  `id` int(11) NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cena` int(10) NOT NULL,
  `img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `velicina_ekrana` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rezolucija` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `procesor` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `radna_memorija` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `interna_memorija` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dual_sim` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mem_kartica` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kamera` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pred_kamera` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `oper_sistem` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bluetooth` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `wifi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `baterija` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `kolicina` int(5) NOT NULL,
  `boja` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dimenzije` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `masa` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `opis` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `opis_tekst` text COLLATE utf8_unicode_ci NOT NULL,
  `id_proizvodjaca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `telefoni`
--

INSERT INTO `telefoni` (`id`, `model`, `cena`, `img`, `velicina_ekrana`, `rezolucija`, `procesor`, `radna_memorija`, `interna_memorija`, `dual_sim`, `mem_kartica`, `kamera`, `pred_kamera`, `oper_sistem`, `bluetooth`, `wifi`, `baterija`, `kolicina`, `boja`, `dimenzije`, `masa`, `opis`, `opis_tekst`, `id_proizvodjaca`) VALUES
(2, 'GALAXY A10', 17499, 'samsung_A10.png', '6.2\"', '1520 x 720', 'Octa-core (2x1.6 GHz & 6x1.35 GHz)', '2 GB', '32 GB', 'Da', 'micro', '13.0 Mpix', '5.0 Mpix', 'Android 9.0 (Pie)', 'v5.0', 'Da', '3400 mAh', 30, 'Plava', '155.6 x 75.8 x 8.1 mm', '168 g', 'SAMSUNG GALAXY A10 32/2GB DS (Plavi) - SM-A105FZBUSEE, 6.2\", 2 GB, 13.0 Mpix, 32 GB', 'Telefon sa ekranom od 6,2 inča u HD+ rezoluciji koji ćeš sa uživanjem gledati. Bez obzira da li je reč o video snimcima ili igrama, uz Infinity-V displej koji ima Galaxy A10 iskusićeš realnost akcije na potpuno nov način. Proveri koliko daleko doseže uživanje uz ekran sa „V” izrezom. Slikaj fotografije onako kako ti želiš koristeći zadnju kameru od 13 MPix i prednju kameru od 5 MPix, i uživaj u svojoj kolekciji slika. Potreban je samo jedan dodir na dugme i sve tvoje uspomene su sačuvane: žive, svetle i jasne. A osmojezgarni procesor prati tvoj ritam i pobrinuće se da sve teče glatko i besprekorno.', 4),
(3, 'iPhone 6s Plus 32GB', 45999, 'iphone_6s_32gb.png', '5.5\"', '1920 x 1080', '1.85 GHz dual-core 64-bit ARMv8-A \"Twister\"', '2 GB', '32 GB', 'Ne', 'Ne', '12.0 Mpix', '5.0 Mpix', 'iOS 10', 'v4.2', 'Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot', '2750 mAh', 30, 'Zlatna', '158.2 x 77.9 x 7.3 mm', '192g', 'APPLE iPhone 6s Plus 32GB (Gold) - MN2X2SE/A 5.5\", 2 GB, 12.0 Mpix, 32 GB', 'APPLE iPhone 6s Plus 32GB (Gold)', 1),
(4, 'P Smart Pr', 38999, 'huawei_p_smart_pro.png', '6.59\"', '2340 x 1080', 'Jezgra: 4 × A73 2.2 GHz + 4 × A53 1.7 GHz', '6 GB', '128 GB', 'Da', 'microSD do 512GB', '48 Mpix + 8 Mpix + 2 Mpix', '16.0 Mpix', 'Android 9.0 (Pie)', 'v4.2', '802.11b/g/n, 2,4 GHz', '4000 mAh', 30, 'Crna', '163.1 x 77.2 x 8.8 mm', '206 g', 'HUAWEI P Smart Pro 6/128GB - Midnight black-Crna - 51094UTV - 6.59\", 6 GB, 48 Mpix + 8 Mpix + 2 Mpix, 128 GB', 'HUAWEI P Smart Pro 6/128GB - Midnight black', 3),
(5, '1S', 12999, 'alcatel_1s.png', '5.5\"', '1440 x 720', 'Cortex-A55 4xA55, 1.6GHz + 4xA55, 1.2GHz', '3 GB', '32 GB', 'Da', 'microSD do 128GB', '13.0 Mpix', '5.0 Mpix', 'Android 9.0 (Pie)', 'v4.2', 'Wi-Fi (802.11a/b/g/n/ac)', '3060 mAh', 30, 'Crna', '147.8 x 70.7 x 8.6 mm', '146 g', 'ALCATEL 1S 3/32GB - Metalic black-Crna - 5024D - 5.5\", 3 GB, 13.0 Mpix + 2.0 Mpix, 32 GB', 'ALCATEL 1S 3/32GB - Metalic black', 2),
(6, '3X 2019 5048Y', 19999, 'alcatel_3x_2019.png', '6.52\"', '1600 x 720', 'Octa Core 269 ppi', '4 GB', '64 GB', 'Da', 'microSD do 128GB', '16 Mpix + 8 Mpix + 5 Mpix', '8.0 Mpix', 'Android 9.0 (Pie)', 'v4.2', 'Wi-Fi (802.11a/b/g/n/ac)', '4000 mAh', 30, 'Crna', '164.9 x 75.8 x 8.4 mm', '178 g', 'ALCATEL 3X 2019 5048Y 4/64GB - Black-Crna - 6.52\", 4 GB, 16 Mpix + 8 Mpix + 5 Mpix, 64 GB', 'ALCATEL 3X 2019 5048Y 4/64GB - Black', 2),
(7, 'GALAXY A51', 44999, 'galaxy_a51.png', '6.5\"', '2400 x 1080', 'Octa-core (4x2.3 GHz Cortex-A73 & 4x1.7 GHz Cortex-A53)', '4 GB', '128 GB', 'Ne', 'microSD do 1TB', '48.0 Mpix + 12.0 Mpix + 5.0 Mpix + 5.0 Mpix', '32.0 Mpix', 'Android 10', 'v5.0', 'Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot', '4000 mAh', 30, 'Plava', '158.5 x 73.6 x 7.9 mm', '172 g', 'SAMSUNG GALAXY A51 4/128GB - Blue-Plava - 6.5\", 4 GB, 48.0 Mpix + 12.0 Mpix + 5.0 Mpix + 5.0 Mpix, 128 GB', 'SAMSUNG GALAXY A51 4/128GB - Blue-Plava - SM-A515FZBVEUF - 6.5\", 4 GB, 48.0 Mpix + 12.0 Mpix + 5.0 Mpix + 5.0 Mpix, 128 GB', 4),
(8, 'iPhone XR 32GB', 95999, 'iphone_xr_64gb.png', '6.1\"', '1792 x 828', 'ARMv8.3-A 64 bita (2x 2.5 GHz Vortex, 4x 1.59 GHz Tempest)', '3 GB', '64 GB', 'Ne', 'Ne', '12.0 Mpix', '7.0 Mpix', 'iOS 12', 'v5.0', 'Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot', '2940 mAh', 30, 'Naradžansta', '150.9 x 75.7 x 8.3 mm', '194 g', 'APPLE iPhone XR 64GB (Narandžasta - Coral) - MRY82SE/A 6.1\", 3 GB, 12.0 Mpix, 64 GB', 'APPLE iPhone XR 64GB- MRY82SE/A 6.1\", 3 GB, 12.0 Mpix, 64 GB', 1),
(9, 'iPhone XR 128GB', 125499, 'iphone_xr_128gb.png', '6.1\"', '1792 x 828', 'ARMv8.3-A 64 bita (2x 2.5 GHz Vortex, 4x 1.59 GHz Tempest)', '3 GB', '128 GB', 'Ne', 'Ne', '12.0 Mpix', '7.0 Mpix', 'iOS 12', 'v5.0', 'Wi-Fi 802.11 b/g/n or a/b/g/n, Wi-Fi Direct', '2940 mAh', 30, 'Bela', '150.9 x 75.7 x 8.3 mm', '194 g', 'APPLE iPhone XR 128GB (Bela) - MRYD2SE/A 6.1\", 3 GB, 12.0 Mpix, 128 GB', 'APPLE iPhone XR 128GB (Bela) - 6.1\", 3 GB, 12.0 Mpix, 128 GB', 1),
(10, 'GALAXY S10', 87999, 'galaxy_s10.png', '6.1\"', '3040 x 1440', 'ARMv8.2-A 64-bita (2x2.7 GHz Mongoose M4 & 2x2.3 GHz Cortex-A75 & 4x1.9 GHz Cortex-A55)', '8 GB', '128 GB', 'Ne', 'microSD do 512GB', '16.0 Mpix + 12.0 Mpix + 12.0 Mpix', '10.0 Mpix', 'Android 9.0 (Pie)', 'v5.0', 'Wi-Fi 802.11 b/g/n or a/b/g/n, Wi-Fi Direct', '3400 mAh', 30, 'Crveni', '70.4 x 149.9 x 7.8 mm', '157 g', 'SAMSUNG GALAXY S10 8/128GB (Crveni) 6.1\", 8 GB, 16.0 Mpix + 12.0 Mpix + 12.0 Mpix, 128 GB', 'SAMSUNG GALAXY S10 8/128GB (Crveni) 6.1\", 8 GB', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `korpa`
--
ALTER TABLE `korpa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maske`
--
ALTER TABLE `maske`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proizvodjaca` (`id_proizvodjaca`);

--
-- Indexes for table `narudzbine`
--
ALTER TABLE `narudzbine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proizvodjaci`
--
ALTER TABLE `proizvodjaci`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tableti`
--
ALTER TABLE `tableti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proizvodjaca` (`id_proizvodjaca`);

--
-- Indexes for table `telefoni`
--
ALTER TABLE `telefoni`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proizvodjaca` (`id_proizvodjaca`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `korpa`
--
ALTER TABLE `korpa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maske`
--
ALTER TABLE `maske`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `narudzbine`
--
ALTER TABLE `narudzbine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proizvodjaci`
--
ALTER TABLE `proizvodjaci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tableti`
--
ALTER TABLE `tableti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `telefoni`
--
ALTER TABLE `telefoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `maske`
--
ALTER TABLE `maske`
  ADD CONSTRAINT `maske_ibfk_1` FOREIGN KEY (`id_proizvodjaca`) REFERENCES `proizvodjaci` (`id`);

--
-- Constraints for table `tableti`
--
ALTER TABLE `tableti`
  ADD CONSTRAINT `tableti_ibfk_1` FOREIGN KEY (`id_proizvodjaca`) REFERENCES `proizvodjaci` (`id`);

--
-- Constraints for table `telefoni`
--
ALTER TABLE `telefoni`
  ADD CONSTRAINT `telefoni_ibfk_1` FOREIGN KEY (`id_proizvodjaca`) REFERENCES `proizvodjaci` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
