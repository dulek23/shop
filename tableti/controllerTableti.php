<?php
    
    require_once '../proizvodjaci/DAOProizvodjaci.php';
    require_once './DAOTableti.php';
    require_once '../korpa/DAOKorpa.php';

    class controllerTableti{
        
        function getProizvodjaci(){
            $dao=new DAOProizvodjaci();
            $proizvodjaci=$dao->getAllProizvodjaci();
            return $proizvodjaci;
        }
 
        function getTableti(){
            $dao=new DAOTableti();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $tab=$dao->getAllTableti();
            include './viewPrikazTableta.php';
        }
        
        function getTabletiDetalji(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOTableti();
            $tablet=$dao->getTabletById($id);
            include './viewTabletiDetaljnije.php';
        }
        
        function getTabletiByProizvodjac(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOTableti();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $tab=$dao->getTabletByIdProizvodjaca($id);
            include './viewPrikazTableta.php';
        }
        
        function getCountTabletiByProizvodjac($id){
            $dao=new DAOTableti();
            $br=$dao->getBrojTabByProiz($id);
            return $br;
        }

    }
?>

