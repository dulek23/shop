<?php
require_once '../config/db.php';

class DAOTableti {
	private $db;

	private $GETCOUNTTABLETIBYPROIZV = "SELECT COUNT(tableti.id) 'broj' FROM proizvodjaci JOIN tableti ON proizvodjaci.id=tableti.id_proizvodjaca WHERE proizvodjaci.id=?";
        private $GETALLTABLETI = "SELECT tableti.*, proizvodjaci.naziv FROM tableti JOIN proizvodjaci ON proizvodjaci.id=tableti.id_proizvodjaca";
        private $GETTABLETBYID = "SELECT tableti.*, proizvodjaci.naziv FROM tableti JOIN proizvodjaci ON proizvodjaci.id=tableti.id_proizvodjaca WHERE tableti.id=?";
        private $GETTABLETIBYIDPROIZV= "SELECT tableti.*, proizvodjaci.naziv FROM tableti JOIN proizvodjaci ON proizvodjaci.id=tableti.id_proizvodjaca WHERE tableti.id_proizvodjaca=?";
        private $SELECTKOLICINA = "SELECT kolicina 'kol' FROM tableti WHERE id=?";
        private $UPDATEKOLICINA = "UPDATE tableti SET kolicina=kolicina-? WHERE id=?";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getBrojTabByProiz($id){

		$statement = $this->db->prepare($this->GETCOUNTTABLETIBYPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getAllTableti(){
                $statement = $this->db->prepare($this->GETALLTABLETI);
                $statement->execute();
		$result = $statement->fetchAll();
		return $result;
        }
        
        public function getTabletById($id){

		$statement = $this->db->prepare($this->GETTABLETBYID);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getTabletByIdProizvodjaca($id){

		$statement = $this->db->prepare($this->GETTABLETIBYIDPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
        
        public function getKolicinaTab($id){

		$statement = $this->db->prepare($this->SELECTKOLICINA);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function updateKolicinaTab($kolicina, $id){
                $statement= $this->db->prepare($this->UPDATEKOLICINA);
                $statement->bindValue(1, $kolicina);
                $statement->bindValue(2, $id);
                $statement->execute();
        }
}
?>
