<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
    <title>Online prodaja i isporuka - shop u Vašem mestu - Mobile Shop</title>
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="d-flex justify-content-center">
                                <div class="d-flex justify-content-center"><img src="../img/baner1.jpg" class="d-block w-100" alt="baner1"></div>
                            </div>
                        </div>
                        
                        <div class="carousel-item">
                            <div class="d-flex justify-content-center">
                                <div class="d-flex justify-content-center"><img src="../img/baner2.jpg" class="d-block w-100" alt="..."></div>
                            </div>
                        </div>
                        
                        <div class="carousel-item">
                            <div class="d-flex justify-content-center">
                                <div class="d-flex justify-content-center"><img src="../img/baner3.jpg" class="d-block w-100" alt="..."></div>
                            </div>
                        </div>
                    </div>
                    
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                </div>
                
            </div>
        </div>
        
        <hr>
        
        <div class="row">
            <div class="col-md-6 col-sm-12" id="kolona_padding">
                <div class="d-flex justify-content-center pt-2 pb-2 pocetna_str_meni_border">
                    <span class="pocetna_meni_title">Izdvajamo iz ponude</span>
                </div>
                
                <div class="d-flex pt-3 pb-3 pocetna_ponuda_div">
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/telefoni/samsung_A10.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../telefoni/?action=detalji&id=2" class="pocetna_ponuda_title">
                                    <span>Samsung Galaxy A10</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>17.499 din.</span></div>
                    </div>
                    
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/telefoni/alcatel_1s.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../telefoni/?action=detalji&id=5" class="pocetna_ponuda_title">
                                    <span>ALCATEL 1S</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>12.499 din.</span></div>
                    </div>
                </div>
                
                <div class="d-flex pt-3 pb-3 pocetna_ponuda_div">
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/tableti/huawei_mediapad_t3.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../tableti/?action=detalji&id=2" class="pocetna_ponuda_title">
                                    <span>HUAWEI Mediapad T3</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>17.999 din.</span></div>
                    </div>
                    
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/tableti/ipad7.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../tableti/?action=detalji&id=3" class="pocetna_ponuda_title">
                                    <span>APPLE iPad 7</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>53.299 din.</span></div>
                    </div>
                </div>
            </div>
            
            
            <div class="col-md-6 col-sm-12" id="kolona_padding">
                <div class="d-flex justify-content-center pt-2 pb-2 pocetna_str_meni_border">
                    <span class="pocetna_meni_title">Najnovije</span>
                </div>
                
                <div class="d-flex pt-3 pb-3 pocetna_ponuda_div">
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/telefoni/alcatel_3x_2019.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../telefoni/?action=detalji&id=6" class="pocetna_ponuda_title">
                                    <span>ALCATEL 3X 2019</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>19.999 din.</span></div>
                    </div>
                    
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/telefoni/iphone_xr_64gb.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../telefoni/?action=detalji&id=8" class="pocetna_ponuda_title">
                                    <span>APPLE iPhone XR 64GB</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>95.999 din.</span></div>
                    </div>
                </div>
                
                <div class="d-flex pt-3 pb-3 pocetna_ponuda_div">
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/tableti/samsung_tab_s6.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../telefoni/?action=detalji&id=2" class="pocetna_ponuda_title">
                                    <span>SAMSUNG Galaxy Tab S6</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>97.999 din.</span></div>
                    </div>
                    
                    <div class="pocetna_ponuda_div_half">
                        
                        <div class="d-flex justify-content-center"><img src="../img/maske/a50_roze.png" class="pocetna_img"></div>
                            <div class="d-flex justify-content-center">
                                <a href="../maske/?action=detalji&id=7" class="pocetna_ponuda_title">
                                    <span>SAMSUNG A50 zaštitna maska</span></div>
                                </a>
                            <div class="d-flex justify-content-center"><span>1.999 din.</span></div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" style="padding: 0px 0px;">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>