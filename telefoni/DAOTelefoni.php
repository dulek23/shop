<?php
require_once '../config/db.php';

class DAOTelefoni {
	private $db;

	private $GETCOUNTTELEFONIBYPROIZV = "SELECT COUNT(telefoni.id) 'broj' FROM proizvodjaci JOIN telefoni ON proizvodjaci.id=telefoni.id_proizvodjaca WHERE proizvodjaci.id=?";
        private $GETALLTELEFONI = "SELECT telefoni.*, proizvodjaci.naziv FROM telefoni JOIN proizvodjaci ON proizvodjaci.id=telefoni.id_proizvodjaca ORDER BY telefoni.id";
        private $GETTELEFONBYID = "SELECT telefoni.*, proizvodjaci.naziv FROM telefoni JOIN proizvodjaci ON proizvodjaci.id=telefoni.id_proizvodjaca WHERE telefoni.id=?";
        private $GETTELEFONIBYIDPROIZV= "SELECT telefoni.*, proizvodjaci.naziv FROM telefoni JOIN proizvodjaci ON proizvodjaci.id=telefoni.id_proizvodjaca WHERE telefoni.id_proizvodjaca=?";
        private $SELECTKOLICINA = "SELECT kolicina 'kol' FROM telefoni WHERE id=?";
        private $UPDATEKOLICINA = "UPDATE telefoni SET kolicina=kolicina-? WHERE id=?";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getBrojTelByProiz($id){

		$statement = $this->db->prepare($this->GETCOUNTTELEFONIBYPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getAllTelefoni(){
                $statement = $this->db->prepare($this->GETALLTELEFONI);
                $statement->execute();
		$result = $statement->fetchAll();
		return $result;
        }
        
        public function getTelefonById($id){

		$statement = $this->db->prepare($this->GETTELEFONBYID);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function getTelefonByIdProizvodjaca($id){

		$statement = $this->db->prepare($this->GETTELEFONIBYIDPROIZV);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
        
        public function getKolicinaTel($id){

		$statement = $this->db->prepare($this->SELECTKOLICINA);
                $statement->bindValue(1, $id);
		$statement->execute();
		$result = $statement->fetch();
		return $result;
	}
        
        public function updateKolicinaTel($kolicina, $id){
                $statement= $this->db->prepare($this->UPDATEKOLICINA);
                $statement->bindValue(1, $kolicina);
                $statement->bindValue(2, $id);
                $statement->execute();
        }
        
}
?>
