<?php
    
    require_once '../proizvodjaci/DAOProizvodjaci.php';
    require_once './DAOTelefoni.php';
    require_once '../korpa/DAOKorpa.php';

    class controllerTelefoni{
        
        function getProizvodjaci(){
            $dao=new DAOProizvodjaci();
            $proizvodjaci=$dao->getAllProizvodjaci();
            return $proizvodjaci;
        }
        
        function getTelefoni(){
            $dao=new DAOTelefoni();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $tel=$dao->getAllTelefoni();
            include './viewPrikazTelefona.php';
        }
        
        function getTelefoniDetalji(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOTelefoni();
            $telefon=$dao->getTelefonById($id);
            include './viewTelefoniDetaljnije.php';
        }
        
        function getTelefoniByProizvodjac(){
            $id= isset($_GET['id'])? $_GET['id']:"";
            $dao=new DAOTelefoni();
            $dao1=new DAOProizvodjaci();
            $proizvodjaci=$dao1->getAllProizvodjaci();
            $tel=$dao->getTelefonByIdProizvodjaca($id);
            include './viewPrikazTelefona.php';
        }
        
        function getCountTelefoniByProizvodjac($id){
            $dao=new DAOTelefoni();
            $br=$dao->getBrojTelByProiz($id);
            return $br;
        }
  
    }
?>
