<?php 
    $sifra= isset($sifra)? $sifra:"";
    require_once './controllerNarudzbine.php';
    $nc=new controllerNarudzbine();
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container-fluid">
        
        <!-- Header -->
    	<div class="row">
    		<div class="col-12" id="kolona_padding">
                    <header><?php include '../template/header.php';?></header>
    		</div>
    	</div>
        
        <!-- Nav -->
        <div class="row">
    		<div class="col-12" id="kolona_padding">
                    <nav><?php include '../template/nav.php';?></nav>
    		</div>
    	</div>
        
        <!-- Content -->
        <?php if($red['broj']==0){ ?>
        <div class="row">
            <div class="col-12">
                <div style="margin: auto;">
                    <div class="d-flex justify-content-center pt-5"><img src="../img/cart.svg" id="korpa_empty_img"></div>
                    <div class="d-flex justify-content-center"><span>Nemate naručenih artikala.</span></div>
                    <div class="d-flex justify-content-center pb-5"><span>Kliknite <a href="../pocetna/viewPocetnaStranica.php" id="korpa_empty_link">ovde</a> da nastavite sa kupovinom.</span></div>
                </div>
            </div>
        </div>
        
        <?php }else{?>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive-md pt-5">
                    <table class="table table-bordered table-striped">
                        <tr class="table-primary">
                            <th>Šifra</th>
                            <th>Model</th>
                            <th>Cena modela</th>
                            <th>Količina</th>
                            <th>Ukupno</th>
                        </tr>
                        <?php foreach ($narudzbine as $n){ ?>
                        <?php
                            $sifra=$nc->sifraProizvoda($n['kategorija'], $n['id_proizvoda']);
                        ?>
                        
                        <tr>
                            <td><?php echo $sifra; ?></td>
                            <td><?php echo $n['model']; ?></td>
                            <td><?php echo $n['cena']; ?></td>
                            <td><?php echo $n['kolicina']; ?></td>
                            <td><?php echo $n['ukupno']; ?></td>
                        </tr>
                        <?php }?>
                    </table>
                </div>
            </div>
        </div>
        <?php }?>
        
        <!-- Footer -->
        <div class="row">
            <div class="col-12" id="kolona_padding">
                <footer><?php include '../template/footer.php'; ?></footer>
            </div>
        </div>
    </div>    
</body>
</html>