<?php
    
    require_once './DAONarudzbine.php';
    require_once '../korpa/controllerKorpa.php';
    require_once '../telefoni/DAOTelefoni.php';
    require_once '../tableti/DAOTableti.php';
    require_once '../maske/DAOMaske.php';
    require_once '../korpa/DAOKorpa.php';

    
    class controllerNarudzbine{
        
        function insertNarudzbina(){
            if(isset($_SESSION['id'])){
            $dao=new DAONarudzbine();
            $kc=new controllerKorpa();
            $dao->insertInNarudzbine();
            $daotel=new DAOTelefoni();
            $daotab=new DAOTableti();
            $daomas=new DAOMaske();
            $id_korisnika=$_SESSION['id'];
            //$narudzbine=$dao->selectAll();
            $daokor=new DAOKorpa();
            $korpa=$daokor->selectAll($id_korisnika);
            foreach ($korpa as $k){
                if($k['kategorija']=="telefoni"){
                    $id=$k['id_proizvoda'];
                    $kolicina=$k['kolicina'];
                    $daotel->updateKolicinaTel($kolicina, $id);
                }else if($k['kategorija']=="tableti"){
                    $id=$k['id_proizvoda'];
                    $kolicina=$k['kolicina'];
                    $daotab->updateKolicinaTab($kolicina, $id);
                }else if($k['kategorija']=="maske"){
                    $id=$k['id_proizvoda'];
                    $kolicina=$k['kolicina'];
                    $daomas->updateKolicinaMas($kolicina, $id);
                }
            }
            $kc->truncateKorpa();
            $narudzbine=$dao->selectAll($id_korisnika);
            $red=$dao->countIdNarudzbine($id_korisnika);
            include './viewNarudzbine.php';
            }
        }
        
        function selectAll(){
            if(isset($_SESSION['id'])){
            $dao=new DAONarudzbine();
            $id=$_SESSION['id'];
            $narudzbine=$dao->selectAll($id);
            $red=$dao->countIdNarudzbine($id);
            include './viewNarudzbine.php';
            }
        }
        
        function sifraProizvoda($kategorija, $id_proizvoda){
           if($kategorija=="telefoni"){
                $sifra=100000+$id_proizvoda;
            }else if($kategorija=="tableti"){
                $sifra=200000+$id_proizvoda;
            }else if($kategorija=="maske"){
                $sifra=300000+$id_proizvoda;
            }
            return $sifra;
        }
        
    }
?>

