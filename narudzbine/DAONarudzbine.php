<?php
require_once '../config/db.php';

class DAONarudzbine {
	private $db;

        private $INSERTNARUDZINE = "INSERT INTO narudzbine (kategorija, kolicina, id_korisnika, id_proizvoda, cena, model, ukupno) SELECT kategorija, kolicina, id_korisnika, id_proizvoda, cena, model, ukupno FROM korpa;";
        private $SELECTALL = "SELECT * FROM narudzbine WHERE id_korisnika=?";
        private $COUNTROWSNARUDZBINE = "SELECT COUNT(id) 'broj' FROM narudzbine WHERE id_korisnika=?";

        public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function insertInNarudzbine(){

		$statement = $this->db->prepare($this->INSERTNARUDZINE);
		$statement->execute();
	}
        
        public function selectAll($id){
                $statement = $this->db->prepare($this->SELECTALL);
                $statement->bindValue(1, $id);
                $statement->execute();
		$result = $statement->fetchAll();
		return $result;
        }
        
        public function countIdNarudzbine($id){
                $statement= $this->db->prepare($this->COUNTROWSNARUDZBINE);
                $statement->bindValue(1, $id);
                $statement->execute();
                $result = $statement->fetch();
                return $result;
        }
}
?>

